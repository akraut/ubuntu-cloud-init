#!/bin/bash

###############################################################################
# Setup Ubuntu 18.04 Docker host VMs in OpenStack
###############################################################################

# I. Users and Groups, SSH, Basic folders -> Recreate puppetized CentOS 7 VMs
###############################################################################
addgroup --gid 2735 sf
useradd --create-home --shell /bin/bash --uid 14806 --gid 2735 --comment SFTNight sftnight
mkdir /home/sftnight/.ssh
cat > /home/sftnight/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzUocu36qfqX7fqgVSXgdyP8bGEM153+fY7HLrQBXnUqD1r2lS72F6cLMbJCYsPumjGq6bi54zqjRyYgy59Q15VzgSkrjG3iwcCDYsl5LfYso/VnIkUxFe2Eggex2UKX84sqzQ/vWTBhfTcJz2/IGKa+JhNi1mkVcYh35BN1tVUdYN9/6KlgQ603ac/r3hk1fMWUB7xKutILBidk4bMoQMhI1ET16FPNG/zyUSY2GIKkWiDMJgR4s8+FVmqABMvOuXCF/qk/Iewu9dQrpnzuPHUyZBwg2ow4RGWmu3JPC2cOZgEErolN1qkSFVcCYtKLAsCinZrui4G5LmerNVrzlgw== Jenkins PH-SFT
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAovmtriOvtihA+Nk7k+Dt2yDWMJxJaDTn89refaStxHOtjYo9L8nL4sspJmpKDdszQdB9xqEGWjZ7GToXdc8D5cuu+BrqZzvIXf2En07QIe8zKxWsaVgmsJHPNVXgbtc34+g82/f+PgbHmOr3nK7n4i3laoNn8A6QUHjuAuz6S4Ou0HCyeVKYPPpf8DB9UrRUMj+13CAeRlSj0i5zfsjb+vMhMLgdkOEKYnAn8HD0pez0fMO1q8UCSzQgBGGOeCduJTu8MaiblIaZcnGx6QWd2v7aDfj6gu58gmg3Yz9Ysl0sZW5+bmDxifBG1W0JoEjWmrW/Vb83omt+SNbk9KYhVQ== sftnight@cern.ch
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDtV02Nkq0zuUQ9nLaffk+XEb0D79S2fiV/MsUDlPGdOTe0BKlDdUfiG1yEFaFQSHhFyUkwvh6/sC85ipANMO22ldp1z/tMU7BeO8OQKEI2OA6A6/t0MXNM3WDMoMrKC1BdYZMhkBnV/2fo3BQ/UmLmz4PH2YYhcrU1y2Sh9QOyuMaqz8jEtdn1qDvnrsPuPXUaXckDb2cJSh6xSEJEkhA16mjZm+bzzGKSQlzrXCT1taPWC0Dvz+Z8TzKFD4+Qnn7EeK/V2hY6xHzjpNAr7jCJDAJrlV92pn52nU566yJhK3TaH6WP7bF2jLdfRq+wPcKhtouXbJkAYyNbDZ3I9Z3 sftnight@epsft-jenkins.cern.ch
EOF
chmod 600 /home/sftnight/.ssh/authorized_keys
chown -R sftnight:sf /home/sftnight/.ssh/

mkdir /ccache
cat > /ccache/ccache.conf << EOF
cache_dir = /ccache
cache_dir_levels = 8
max_files = 0
max_size = 20.0G
EOF
chown -R sftnight:sf /ccache/

mkdir -p /ec/conf/
chmod -R 770 /ec/
chown -R sftnight:sf /ec/

mkdir /mnt/build
ln -s /mnt/build /build

mkdir /mnt/docker
ln -s /mnt/docker /docker


# II. Base Packages
###############################################################################
apt update
apt install -y \
    apt-transport-https \
    ca-certificates \
    cmake \
    curl \
    git \
    gnupg-agent \
    lsb-release \
    openjdk-8-jre-headless \
    software-properties-common \
    tmux \
    tree \
    unattended-upgrades \
    vim

# Configure unattended upgrades
sed -i "s|//\t\"\${distro_id}:\${distro_codename}-updates\";|\t\"\${distro_id}:\${distro_codename}-updates\";|" /etc/apt/apt.conf.d/50unattended-upgrades
sed -i "s|//Unattended-Upgrade::Automatic-Reboot \"false\";|Unattended-Upgrade::Automatic-Reboot \"true\";|" /etc/apt/apt.conf.d/50unattended-upgrades
sed -i "s|//Unattended-Upgrade::Automatic-Reboot-Time \"02:00\";|Unattended-Upgrade::Automatic-Reboot-Time \"22:50\";|" /etc/apt/apt.conf.d/50unattended-upgrades

# Enable the timers for the unattended upgrades
cat > /etc/apt/apt.conf.d/20auto-upgrades << EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
EOF


# III. CVMFS
###############################################################################
# https://cernvm.cern.ch/portal/filesystem/quickstart
# https://cernvm.cern.ch/portal/filesystem/downloads
wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
dpkg -i cvmfs-release-latest_all.deb
rm -f cvmfs-release-latest_all.deb
apt update
apt install -y cvmfs cvmfs-config-default
cvmfs_config setup
cat > /etc/cvmfs/default.local << EOF
CVMFS_DNS_MIN_TTL='300'
CVMFS_QUOTA_LIMIT='10000'
CVMFS_HTTP_PROXY='http://ca-proxy-meyrin.cern.ch:3128;http://ca-proxy.cern.ch:3128'
CVMFS_CACHE_BASE='/var/lib/cvmfs'
export CMS_LOCAL_SITE=T2_CH_CERN
CVMFS_REPOSITORIES='sft.cern.ch,sft-nightlies.cern.ch'
EOF
cvmfs_config probe

# Remove CVMFS from autofs as we mount cvmfs manually but still need autofs for EOS
sed -i "s|/cvmfs /etc/auto.cvmfs|# /cvmfs /etc/auto.cvmfs|" /etc/auto.master


# IV. Kerberos
###############################################################################
apt install -y krb5-user libpam-krb5 libpam-ccreds auth-client-config
cat > /etc/krb5.conf << EOF
; AD  : This Kerberos configuration is for CERN's Active Directory realm
; The line above this is magic and is used by cern-config-keytab. Do not remove.

[libdefaults]
default_realm = CERN.CH
ticket_lifetime = 25h
renew_lifetime = 120h
forwardable = true 
proxiable = true
default_tkt_enctypes = arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc
chpw_prompt = true
allow_weak_crypto = true
 
[appdefaults]
pam = {
    external = true
    krb4_convert = false
    krb4_convert_524 = false
    krb4_use_as_req = false
}

[domain_realm]
.cern.ch = CERN.CH

[realms]
CERN.CH  = {
  default_domain = cern.ch
  kpasswd_server = cerndc.cern.ch
  admin_server = cerndc.cern.ch
  kdc = cerndc.cern.ch
  v4_name_convert = {
    host = {
      rcmd = host
    }
  }
}
EOF


# V. Docker
###############################################################################
# https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt install -y docker-ce docker-ce-cli containerd.io
usermod -a -G docker ubuntu
usermod -a -G docker sftnight


# VI. XRootD and EOS fuseX client
###############################################################################
# http://eos-docs.web.cern.ch/eos-docs/quickstart/ubuntu.html
curl -fsSL http://storage-ci.web.cern.ch/storage-ci/storageci.key | apt-key add -
add-apt-repository "deb [arch=amd64] http://storage-ci.web.cern.ch/storage-ci/debian/xrootd bionic release"
add-apt-repository "deb [arch=amd64] http://storage-ci.web.cern.ch/storage-ci/debian/eos/citrine/ bionic tag"
apt update
apt install -y xrootd-client eos-client eos-fuse

# Configuration: https://cern.service-now.com/service-portal/article.do?n=KB0003846
mkdir /eos
mkdir /etc/sysconfig

sed -i "s|browse_mode = no|browse_mode = yes|" /etc/autofs.conf
echo "/eos /etc/auto.eos" >> /etc/auto.master

cat > /etc/auto.eos << EOF
user     -fstype=eos  :user
project  -fstype=eos  :project
EOF

cat > /etc/sysconfig/eos.user << EOF
export EOS_FUSE_CACHE_PAGE_SIZE=32768
export EOS_FUSE_CACHE_SIZE=268435456
export EOS_FUSE_DEBUG=0
export EOS_FUSE_FILE_WB_CACHE_SIZE=1048576
export EOS_FUSE_LAZYOPENRW=1
export EOS_FUSE_LOGLEVEL=4
export EOS_FUSE_MGM_ALIAS=eosuser-fuse.cern.ch
export EOS_FUSE_MOUNTDIR=/eos/user/
export EOS_FUSE_NEG_ENTRY_CACHE_TIME=1.0e-09
export EOS_FUSE_NOPIO=1
export EOS_FUSE_PIDMAP=1
export EOS_FUSE_RDAHEAD=1
export EOS_FUSE_RDAHEAD_WINDOW=262144
export EOS_FUSE_RMLVL_PROTECT=2
export EOS_FUSE_SHOW_SPECIAL_FILES=0
export EOS_FUSE_SYNC=1
export EOS_FUSE_USER_KRB5CC=1
export EOS_LOG_SYSLOG=0
export XRD_APPNAME=eos-fuse
export XRD_CONNECTIONRETRY=4096
export XRD_CONNECTIONWINDOW=10
export XRD_DATASERVERTTL=300
export XRD_LOADBALANCERTTL=1800
export XRD_LOGLEVEL=Info
export XRD_REDIRECTLIMIT=5
export XRD_REQUESTTIMEOUT=60
export XRD_STREAMERRORWINDOW=60
export XRD_STREAMTIMEOUT=60
export XRD_TIMEOUTRESOLUTION=1
export XRD_WORKERTHREADS=16
EOF

cat > /etc/sysconfig/eos.project << EOF
export EOS_FUSE_CACHE_PAGE_SIZE=32768
export EOS_FUSE_CACHE_SIZE=268435456
export EOS_FUSE_DEBUG=0
export EOS_FUSE_FILE_WB_CACHE_SIZE=1048576
export EOS_FUSE_LAZYOPENRW=1
export EOS_FUSE_LOGLEVEL=4
export EOS_FUSE_MGM_ALIAS=eosproject-fuse.cern.ch
export EOS_FUSE_MOUNTDIR=/eos/project/
export EOS_FUSE_NEG_ENTRY_CACHE_TIME=1.0e-09
export EOS_FUSE_NOPIO=1
export EOS_FUSE_PIDMAP=1
export EOS_FUSE_RDAHEAD=1
export EOS_FUSE_RDAHEAD_WINDOW=262144
export EOS_FUSE_RMLVL_PROTECT=1
export EOS_FUSE_SHOW_SPECIAL_FILES=0
export EOS_FUSE_SYNC=1
export EOS_FUSE_USER_KRB5CC=1
export EOS_LOG_SYSLOG=0
export XRD_APPNAME=eos-fuse
export XRD_CONNECTIONRETRY=4096
export XRD_CONNECTIONWINDOW=10
export XRD_DATASERVERTTL=300
export XRD_LOADBALANCERTTL=1800
export XRD_LOGLEVEL=Info
export XRD_REDIRECTLIMIT=5
export XRD_REQUESTTIMEOUT=60
export XRD_STREAMERRORWINDOW=60
export XRD_STREAMTIMEOUT=60
export XRD_TIMEOUTRESOLUTION=1
export XRD_WORKERTHREADS=16
EOF
