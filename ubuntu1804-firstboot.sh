#!/bin/bash
set -e

###############################################################################
#  (!) Run this script INTERACTIVELY as user 'ubuntu' on the Ubuntu 18.04     #
#      Docker host VMs. You can connect via SSH public key from OpenStack (!) #
#                                                                             #
#  Hint: The password for the local user 'sftnight' is set automatically via  #
#        Kerberos and you can login as 'sftnight' from lxplus7 after reboot.  #
###############################################################################

# Please specify any working Jenkins node with a valid Kerberos sftnight.keytab
WORKING_JENKINS_NODE="lcgapp-centos7-x86-64-40.cern.ch"

function print_separator() {
    echo
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo
}


###############################################################################
# I. Download sftnight.keytab from another build server (interactive)         #
###############################################################################
echo "Please enter the password for sftnight to download the sftnight.keytab:"
sudo scp -p -o StrictHostKeyChecking=no \
    sftnight@${WORKING_JENKINS_NODE}:/ec/conf/sftnight.keytab \
    /ec/conf/sftnight.keytab
print_separator


###############################################################################
# II. Update software to latest version                                       #
###############################################################################
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
print_separator


###############################################################################
# III. Test Docker setup                                                      #
###############################################################################
docker images
docker run hello-world
docker ps -a
docker system prune -af
print_separator


###############################################################################
# IV. Print basic system information                                          #
###############################################################################

echo "SYSTEM:"
lsb_release -a
echo "Kernel:    $( uname -sr )"
echo "Host name: $( hostname )"
echo "Time zone: $( cat /etc/timezone )"
print_separator

echo "TIMEZONE:"
sudo timedatectl set-timezone Europe/Zurich
timedatectl
print_separator

echo "LOCALE:"
sudo locale-gen en_US.UTF-8
sudo update-locale LANG=en_US.UTF-8
cat /etc/default/locale
localectl
print_separator

echo "HOMEs:"
ls -Alh /home
print_separator


###############################################################################
# V. Setup and test Kerberos                                                  #
###############################################################################
sudo chmod 400 /ec/conf/sftnight.keytab
sudo chown -R sftnight:sf /ec/
echo "KERBEROS:"
export EOS_MGM_URL="root://eosuser.cern.ch"
sudo kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
print_separator

echo "EOS XROOTD:"
sudo xrdfs ${EOS_MGM_URL} ls /eos/project/l/lcg/www
print_separator

echo "EOS FUSE:"
sudo eosfusebind
sudo ls /eos/project/l/lcg/www
print_separator


###############################################################################
# VI. Setup second hard disk (file system, mount)                             #
#     Mount CVMFS via /etc/fstab - not automount                              #
###############################################################################

echo "FILE SYSTEM:"

# Create a ext4 file system and give it the label 'build'
sudo mkfs.ext4 -L build /dev/vdb
print_separator

# Create a ext4 file system and give it the label 'docker'
sudo mkfs.ext4 -L docker /dev/vdc
print_separator

# Add the /tmp definition to /etc/fstab
echo "tmpfs        /tmp                tmpfs  rw,nodev,nosuid,size=10G       0 0" | sudo tee -a /etc/fstab > /dev/null 

# Add the build disk to /etc/fstab
echo "/dev/vdb     /mnt/build          ext4   noatime,nodiratime,user_xattr  0 0" | sudo tee -a /etc/fstab > /dev/null 

# Add the docker disk to /etc/fstab
echo "/dev/vdc     /mnt/docker         ext4   noatime,nodiratime,user_xattr  0 0" | sudo tee -a /etc/fstab > /dev/null 

# Add the CVMFS repositories to /etc/fstab
echo "sft.cern.ch  /cvmfs/sft.cern.ch  cvmfs  defaults,_netdev,nodev         0 0" | sudo tee -a /etc/fstab > /dev/null 
echo "sft-nightlies.cern.ch  /cvmfs/sft-nightlies.cern.ch  cvmfs  defaults,_netdev,nodev  0 0" | sudo tee -a /etc/fstab > /dev/null 

cat /etc/fstab
print_separator

# Stop autofs to avoid interference with CVMFS
sudo systemctl stop autofs
echo "Status autofs: $( systemctl is-active autofs.service )"

# Create the necessary directory structure for a manual mount of CVMFS
sudo mkdir /cvmfs/sft.cern.ch
sudo mkdir /cvmfs/sft-nightlies.cern.ch
sudo chown -R cvmfs:cvmfs /cvmfs/sft.cern.ch
sudo chown -R cvmfs:cvmfs /cvmfs/sft-nightlies.cern.ch

# Try to mount all devices in /etc/fstab
sudo mount -av
print_separator

# Enable autofs for EOS fuseX although we mount CVMFS explicitly
sudo systemctl start autofs
sudo systemctl enable autofs
echo "Status autofs: $( systemctl is-active autofs.service )"
print_separator

# Show CVMFS
echo "CVMFS:"
ls -Arthl /cvmfs
print_separator

ls /cvmfs/sft.cern.ch/lcg/views/
print_separator

ls /cvmfs/sft-nightlies.cern.ch/lcg/views/
print_separator

# Make sftnight owner of /mnt/build
sudo mkdir -p /mnt/build/jenkins
sudo chmod -R 0755 /mnt/build/jenkins
sudo chown -R sftnight:sf /mnt/build
sudo chown -R sftnight:sf /build

echo "ROOT DIRECTORY:"
ls -Alh /
print_separator

echo "MNT DIRECTORY:"
ls -Alh /mnt
print_separator

# Docker configuration to use /mnt/docker as main directory
echo "DOCKER:"
echo "{" | sudo tee -a /etc/docker/daemon.json > /dev/null 
echo "    \"data-root\": \"/mnt/docker\"," | sudo tee -a /etc/docker/daemon.json > /dev/null 
echo "    \"storage-driver\": \"overlay\"" | sudo tee -a /etc/docker/daemon.json > /dev/null 
echo "}" | sudo tee -a /etc/docker/daemon.json > /dev/null 
sudo ls -hAl /docker/
sudo systemctl restart docker
docker run hello-world
docker system prune -af
sudo ls -hAl /docker/
print_separator

# List current file system
echo "CURRENT FILE SYSTEM:"
df -h


###############################################################################
# VII. Reboot                                                                 #
###############################################################################
print_separator
echo "[✓] Finished successfully. Please check the log output above"
echo "    and confirm a reboot of the VM below"
unset REBOOT_CONFIRMATION
while [[ "$REBOOT_CONFIRMATION" != "Y" ]] && [[ "$REBOOT_CONFIRMATION" != "y" ]]
do
    read -p "    Plese confirm the reboot by pressing the 'Y' key: " REBOOT_CONFIRMATION
done
echo "Rebooting ..."
sleep 1s
sudo shutdown -r now
